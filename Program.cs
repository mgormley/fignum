﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace fignum
{
    class Program
    {
        /// <summary>
        /// Takes an int value and validates if the numer is a prime number or not.
        /// </summary>
        /// <param name="intValue">The int value the function will accept.</param>
        /// <returns>bool value if the number is prime or not.</returns>
        public static bool IsPrimeNumber(int intValue)
        {
            if (intValue <= 1) // 1 is not a prime. Return false as this does not need to be calculated.
            {
                return false;
            }

            // Calculate all the possible divisors up to the square root of intValue.
            for (var divisor = 2; divisor * divisor <= intValue; divisor++)
            {
                if (intValue % divisor == 0)
                {
                    return false; // Number is not a prime number.
                }
            }
            return true; // Number is a prime number.
        }

        /// <summary>
        /// Takes List<int> and if the prime number returns false, adds to new List<int>.
        /// </summary>
        /// <param name="intList">The original List<int> that contains all prime and non prime numbers.</param>
        /// <param name="nonPrimeIntList">The new List<int> that contains only non prime numbers.</param>
        public static void RemovePrimeNumbers(List<int> intList, List<int> nonPrimeIntList)
        {
            for (var i = 0; i < intList.Count; i++) // Iterate through intList and check each value.
            {
                var listValue = intList[i];
                if (IsPrimeNumber(listValue) == false)
                {
                    // Add non prime values to new nonPrimeIntList.
                    nonPrimeIntList.Add(listValue);
                }
                else
                {
                    // List out prime number identified.
                    Console.WriteLine($"Prime number {listValue} found, removing from list.");
                }

            }
            Console.WriteLine("\b");
        }

        /// <summary>
        /// Main entry point of the application. Takes a string containing some numbers and applies a system configured filter, followed by a system configured sort.
        /// </summary>
        /// <param name="reason">A brief reason for failure; this forms the subject of the alert message.</param>
        /// <param name="context">Contextual information regarding the error, such as data being processed.</param>
        /// <param name="exception">The exception, if applicable.</param>
        public static void Main()
        {
            // Input values and expected output.
            var inputString = "10 12 14 15 Lorem 11 13 17 ipsum dolor sit amet 19 23 29 31 consectetur 16 18 20 adipiscing elit Phasellus 2 3 5 7 venenatis malesuada  1 4 6 8 9 lectus ";
            Console.WriteLine($"inputString: {inputString} \n");
            var expectedOutput = "1 4 6 8 9 10 12 14 15 16 18 20 adipiscing amet consectetur dolor elit ipsum lectus Lorem malesuada Phasellus sit venenatis";
            Console.WriteLine($"expectedOutput of string: {expectedOutput} \n");

            // Create string list and gather all non numeric values from inputString.
            List<string> stringList = new List<string>();
            Match nonNumbers = Regex.Match(inputString, @"([a-zA-Z]+\s)");
            while (nonNumbers.Success)
            {
                stringList.Add(nonNumbers.Value);
                nonNumbers = nonNumbers.NextMatch();
            }
            Console.WriteLine($"nonNumbers identified: ");
            stringList.ForEach(i => Console.Write("{0}\t", i));
            Console.WriteLine("\n");

            // Create int list and gather all numeric values from inputString.
            var numbers = Regex.Matches(inputString, @"\d+").Select(n => int.Parse(n.Value)).ToList();
            List<int> nonPrimeIntList = new List<int>();

            // Initiate function to remove prime numbers.
            RemovePrimeNumbers(numbers, nonPrimeIntList);
            Console.WriteLine($"numbers output with prime numbers removed: ");
            nonPrimeIntList.ForEach(i => Console.Write("{0}\t", i));
            Console.WriteLine("\n");

            // Sort lists in ascending order.
            stringList.Sort();
            nonPrimeIntList.Sort();

            // Convert to strings and concatenate sorted strings in ascending order.
            var IntListToString = string.Join(" ", nonPrimeIntList);
            var stringListToString = string.Join("", stringList);
            var concatString = IntListToString + " " + stringListToString.Trim();
            Console.WriteLine($"Sorted string in ascending order with prime numbers removed: ");
            Console.WriteLine($"{concatString} \n");

            // Validate that the expected output matches the concatenated string. Pass the result if it matches, fail the result if it does not.
            if (expectedOutput == concatString)
            {
                Console.WriteLine("PASS: Filtered and Sorted output matches expectedOutput.");
            }
            else
            {
                Console.WriteLine("FAIL: Filtered and Sorted output does not match expectedOutput.");
            }

            // Assumptions:
            // Where it states that the string would contain some numbers, I assumed it would also contain other characters.
            // I handled this in the regex for the string to also include letters, however the regex could be amended if the system was likely to become larger in the future or if the brief required it.

            //Note: 
            // I spent the allocated time on this, however if I had additional time to complete, I would have also included unit testing around the functions for more validity.
        }
    }
}